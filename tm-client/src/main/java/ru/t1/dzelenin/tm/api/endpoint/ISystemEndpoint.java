package ru.t1.dzelenin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.request.system.ServerAboutRequest;
import ru.t1.dzelenin.tm.dto.request.system.ServerVersionRequest;
import ru.t1.dzelenin.tm.dto.response.AbstractResponse;

public interface ISystemEndpoint {

    @NotNull
    AbstractResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    AbstractResponse getVersion(@NotNull ServerVersionRequest request);

}

