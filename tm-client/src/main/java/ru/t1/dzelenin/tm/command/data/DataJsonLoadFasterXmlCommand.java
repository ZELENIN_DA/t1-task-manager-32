package ru.t1.dzelenin.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.Domain;
import ru.t1.dzelenin.tm.dto.request.domain.DataJsonLoadFasterXmlRequest;
import ru.t1.dzelenin.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-load-json";

    @Getter
    @NotNull
    private final String description = "Load data from json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        getDomainEndpoint().loadDataJsonFasterXml(new DataJsonLoadFasterXmlRequest());
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }

}

