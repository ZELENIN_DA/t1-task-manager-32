package ru.t1.dzelenin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.project.ProjectShowByIndexRequest;
import ru.t1.dzelenin.tm.dto.response.project.ProjectShowByIndexResponse;
import ru.t1.dzelenin.tm.model.Project;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectShowCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(index);
        @NotNull final ProjectShowByIndexResponse response = getProjectEndpoint().showProjectByIndex(request);
        showProject(response.getProject());
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display project by index.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
