package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.ICommandRepository;
import ru.t1.dzelenin.tm.api.service.ICommandService;
import ru.t1.dzelenin.tm.command.AbstractCommand;

import java.util.Collection;

import java.util.Collection;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandWithArgument() {
        return commandRepository.getCommandWithArgument();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommand() {
        return commandRepository.getTerminalCommand();
    }

}

