package ru.t1.dzelenin.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dzelenin.tm.dto.request.system.ServerAboutRequest;
import ru.t1.dzelenin.tm.dto.request.system.ServerVersionRequest;
import ru.t1.dzelenin.tm.dto.response.system.ServerAboutResponse;
import ru.t1.dzelenin.tm.dto.response.system.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}

