package ru.t1.dzelenin.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.Domain;
import ru.t1.dzelenin.tm.dto.request.domain.DataBackupSaveRequest;
import ru.t1.dzelenin.tm.enumerated.Role;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-save";

    @Getter
    @NotNull
    private final String description = "Save backup to file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Save backup");
        getDomainEndpoint().saveDataBackup(new DataBackupSaveRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }


}

