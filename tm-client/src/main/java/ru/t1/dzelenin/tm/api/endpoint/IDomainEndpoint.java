package ru.t1.dzelenin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.request.domain.*;
import ru.t1.dzelenin.tm.dto.response.domain.*;

public interface IDomainEndpoint {

    @NotNull
    DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxBResponse loadDataJsonJaxb(@NotNull DataJsonLoadJaxBRequest request);

    @NotNull
    DataJsonSaveJaxBResponse saveDataJsonJaxb(@NotNull DataJsonSaveJaxBRequest request);

    @NotNull
    DataXmlLoadJaxBResponse loadDataXmlJaxb(@NotNull DataXmlLoadJaxBRequest request);

    @NotNull
    DataXmlSaveJaxBResponse saveDataXmlJaxb(@NotNull DataXmlSaveJaxBRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request);

}

