package ru.t1.dzelenin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.task.TaskShowByIdRequest;
import ru.t1.dzelenin.tm.dto.response.task.TaskShowByIdResponse;
import ru.t1.dzelenin.tm.model.Task;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskShowCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(id);
        @NotNull final TaskShowByIdResponse response = getTaskEndpoint().showTaskById(request);
        showTask(response.getTask());
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display task by id.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
