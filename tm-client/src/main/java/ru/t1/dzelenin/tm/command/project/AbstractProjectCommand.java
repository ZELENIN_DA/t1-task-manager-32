package ru.t1.dzelenin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.endpoint.IProjectEndpointClient;
import ru.t1.dzelenin.tm.api.service.IProjectService;
import ru.t1.dzelenin.tm.api.service.IProjectTaskService;
import ru.t1.dzelenin.tm.command.AbstractCommand;
import ru.t1.dzelenin.tm.enumerated.Role;


public abstract class AbstractProjectCommand extends AbstractCommand {
    public IProjectEndpointClient getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

