package ru.t1.dzelenin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.request.task.TaskListByProjectIdRequest;
import ru.t1.dzelenin.tm.dto.response.task.TaskListByProjectIdResponse;
import ru.t1.dzelenin.tm.model.Task;
import ru.t1.dzelenin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskListCommand {

    private final String NAME = "task-show-by-project-id";

    private final String DESCRIPTION = "Show task list by project id.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(projectId);
        @NotNull final TaskListByProjectIdResponse response = getTaskEndpoint().listTaskByProjectId(request);
        renderTasks(response.getTasks());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

