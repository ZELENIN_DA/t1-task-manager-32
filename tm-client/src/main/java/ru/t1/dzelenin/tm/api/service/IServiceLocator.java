package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpointClient getAuthEndpoint();

    @NotNull
    IProjectEndpointClient getProjectEndpoint();

    @NotNull
    ITaskEndpointClient getTaskEndpoint();

    @NotNull
    IUserEndpointClient getUserEndpoint();

    @NotNull
    IDomainEndpointClient getDomainEndpoint();

}

