package ru.t1.dzelenin.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.Domain;
import ru.t1.dzelenin.tm.dto.request.domain.DataBinarySaveRequest;
import ru.t1.dzelenin.tm.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-save-bin";

    @Getter
    @NotNull
    private final String description = "Save data to binary file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");
        getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest());
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }

}

