package ru.t1.dzelenin.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dzelenin.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.dzelenin.tm.dto.request.user.UserLoginRequest;
import ru.t1.dzelenin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dzelenin.tm.dto.request.user.UserProfileRequest;
import ru.t1.dzelenin.tm.dto.response.user.UserLoginResponse;
import ru.t1.dzelenin.tm.dto.response.user.UserLogoutResponse;
import ru.t1.dzelenin.tm.dto.response.user.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
