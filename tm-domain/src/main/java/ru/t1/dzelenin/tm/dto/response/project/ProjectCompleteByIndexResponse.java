package ru.t1.dzelenin.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.model.Project;

@NoArgsConstructor
public final class ProjectCompleteByIndexResponse extends AbstractProjectResponse {

    public ProjectCompleteByIndexResponse(@Nullable final Project project) {
        super(project);
    }

}


