package ru.t1.dzelenin.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.response.AbstractUserResponse;
import ru.t1.dzelenin.tm.model.User;

public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
