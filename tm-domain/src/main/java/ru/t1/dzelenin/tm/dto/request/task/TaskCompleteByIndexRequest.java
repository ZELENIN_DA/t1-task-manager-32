package ru.t1.dzelenin.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.AbstractIndexRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCompleteByIndexRequest extends AbstractIndexRequest {

    public TaskCompleteByIndexRequest(@Nullable final Integer index) {
        super(index);
    }

}

