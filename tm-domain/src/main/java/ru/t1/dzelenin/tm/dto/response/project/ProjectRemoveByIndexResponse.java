package ru.t1.dzelenin.tm.dto.response.project;

import lombok.NoArgsConstructor;
import ru.t1.dzelenin.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class ProjectRemoveByIndexResponse extends AbstractProjectResponse {
}
