package ru.t1.dzelenin.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.AbstractIdRequest;
import ru.t1.dzelenin.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIdRequest extends AbstractIdRequest {

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(@Nullable final String id, @Nullable final Status status) {
        super(id);
        this.status = status;
    }

}
