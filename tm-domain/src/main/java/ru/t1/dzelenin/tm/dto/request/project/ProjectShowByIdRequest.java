package ru.t1.dzelenin.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.AbstractIdRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowByIdRequest extends AbstractIdRequest {

    public ProjectShowByIdRequest(@Nullable final String id) {
        super(id);
    }

}
