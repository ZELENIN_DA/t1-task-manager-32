package ru.t1.dzelenin.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
