package ru.t1.dzelenin.tm.dto.request.domain;

import lombok.NoArgsConstructor;
import ru.t1.dzelenin.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataYamlLoadFasterXmlRequest extends AbstractUserRequest {
}