package ru.t1.dzelenin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public AbstractIndexRequest(@Nullable final Integer index) {
        this.index = index;
    }

}
