package ru.t1.dzelenin.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.AbstractIdRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskStartByIdRequest extends AbstractIdRequest {

    public TaskStartByIdRequest(@Nullable final String id) {
        super(id);
    }

}


