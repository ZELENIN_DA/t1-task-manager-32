package ru.t1.dzelenin.tm.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @Nullable
    protected String userId = null;

    @NotNull
    protected final Socket socket;

    public AbstractServerSocketTask(
            @NotNull Server server,
            @NotNull Socket socket
    ) {
        super(server);
        this.socket = socket;
    }

    public AbstractServerSocketTask(
            @NotNull Server server,
            @NotNull Socket socket,
            @Nullable final String userId
    ) {
        super(server);
        this.socket = socket;
        this.userId = userId;
    }

}

