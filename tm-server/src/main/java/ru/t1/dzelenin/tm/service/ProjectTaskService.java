package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.IProjectRepository;
import ru.t1.dzelenin.tm.api.repository.ITaskRepository;
import ru.t1.dzelenin.tm.api.service.IProjectTaskService;
import ru.t1.dzelenin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dzelenin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dzelenin.tm.exception.field.IndexIncorrectException;
import ru.t1.dzelenin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dzelenin.tm.exception.field.TaskIdEmptyException;
import ru.t1.dzelenin.tm.exception.field.UserIdEmptyException;
import ru.t1.dzelenin.tm.model.Project;
import ru.t1.dzelenin.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public void bindTaskToProject(@NotNull final String userId,
                                  @NotNull final String projectId,
                                  @NotNull final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    public void unbindTaskFromProject(@NotNull final String userId,
                                      @NotNull final String projectId,
                                      @NotNull final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

    public void removeProjectById(@NotNull final String userId,
                                  @NotNull final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeOneById(task.getId());
        projectRepository.removeOneById(projectId);
    }

    @Override
    public void removeProjectByIndex(@Nullable final String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= projectRepository.getSize()) throw new IndexIncorrectException();
        @NotNull final Project project = projectRepository.findOneByIndex(userId, index);
        removeProjectById(userId, project.getId());
    }

}
